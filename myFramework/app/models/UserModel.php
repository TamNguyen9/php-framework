<?php

namespace app\models;

class UserModel extends Model
{

    // lay all ban ghi de show ra
    public function modelGetList()
    {
        $data = $this->selectAll("users");
        return $data;

    }

    //lay 1 ban ghi
    public function modelGetRecord()
    {
        $record = $this->selectWhere("users",);
    }

    // lay ban ghi da co san de check voi ban ghi nhap vao
    public function modelCreate()
    {
        $name = $_POST["name"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        //ma hoa password
        $password = md5($password);
        //update name
        $query = $this->selectById("users", "email", $email);

        if (count($query) > 0) {
            return false;
        }
//        $dateSave = [
//            [
//                "type" => "char",
//                "value" => $name
//            ],
//            [
//                "type" => "char",
//                "value" => $email
//            ],
//            [
//                "type" => "char",
//                "value" => $password
//            ]
//        ];
//        $insert = $this->insertInto("users", $dateSave);
        $insert = $this->conn->prepare("insert into users set name=:var_name,email=:var_email,password=:var_password");
        $insert->execute(["var_name" => $name, "var_email" => $email, "var_password" => $password]);
        return true;
    }

    public function updateDetailUser($id, $name)
    {
        //chuan bi truy van
        $query = $this->conn->prepare("update users set name=:var_name where id=:var_id");
        //thuc thi truy van, co truyen tham so vao cau lenh sql
        $query->execute(["var_id" => $id, "var_name" => $name]);
        return true;
    }

    public function modelDelete($id)
    {
        $query = $this->conn->prepare("delete from users where id=:var_id");
        $query->execute(["var_id" => $id]);
    }
}



