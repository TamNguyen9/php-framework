<?php

namespace app\models;

use database\DB;

class Model
{

    public $connectFile;
    public $dataSet = NULL;
    public $sqlQuery = NULL;
    public $conn;

    // biến kết nối csdl
    public function __construct()
    {
        $this->connectFile = new DB();
        $this->conn = $this->connectFile->dbConnect();
    }

    // selectAll
    public function selectAll($tableName)
    {
        //cau lenh truy van
        $this->sqlQuery = "SELECT * FROM " . $tableName;
        //thuc hien truy van
        $query = $this->conn->query($this->sqlQuery);
        //tra ve all ban ghi
        return $query->fetchAll();
    }

    // selectAll
    public function find($tableName, $id)
    {
        //cau lenh truy van
        $this->sqlQuery = "SELECT * FROM " . $tableName . " WHERE id = " . $id;
        // lay bien ket noi csdl
        $query = $this->conn->query($this->sqlQuery);
        //tra ve all ban ghi
        return $query->fetch();
    }

    // select có điều kiện
    public function selectWhere($tableName, $rowName, $operator, $value, $valueType)
    {
        //cau lenh truy van
        $this->sqlQuery = "SELECT * FROM " . $tableName . " WHERE " . $rowName . " " . $operator . " ";
        //kiem tra type của value nhap vao va set cau lenh truy van
        if ($valueType == 'int') {
            $this->sqlQuery .= $value;
        } else if ($valueType == 'char') {
            $this->sqlQuery .= "'" . $value . "'";
        }
        //thuc hien truy van
        $query = $this->conn->query($this->sqlQuery);
        //tra ve nhieu ban ghi
        return $query->fetchAll();
    }

    public function selectById($tableName, $rowName, $value)
    {
        $this->sqlQuery = "SELECT * FROM " . $tableName . " WHERE " . $rowName . " = " . "'" . $value . "'";
        $query = $this->conn->query($this->sqlQuery);
        return $query->fetchAll();
    }

    public function insertInto($tableName, $values)
    {
        $this->sqlQuery = "INSERT INTO " . $tableName . " VALUES (";
        $i = 0;
        while ($values[$i]["val"] != NULL && $values[$i]["type"] != NULL) {
            if ($values[$i]["type"] == "char") {
                $this->sqlQuery .= "'" . $values[$i]["val"] . "'";
            } else if ($values[$i]["type"] == 'int') {
                $this->sqlQuery .= $values[$i]["val"];
            }
            $i++;
            if ($values[$i]["val"] != NULL) {
                $this->sqlQuery .= ',';
            }
        }

        $this->sqlQuery .= ')';
        // thuc hien truy van
        return $this->conn->query($this->sqlQuery);
    }
}

?>