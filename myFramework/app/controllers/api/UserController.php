<?php

namespace app\controllers\api;

use app\controllers\Controller;

use app\models\UserModel;

class UserController extends Controller
{

    public function __construct()
    {
        //echo "<br> this text is from UserController";
    }

    public function read()
    {
        $recordPerPage = 4;
        $getList = new \app\models\UserModel();
        $data = $getList->modelGetList();
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data);
    }

    public function addUser()
    {
        $uniqueEmail = new UserModel();
        $checkEmail = $uniqueEmail->modelCreate();
        if (!$checkEmail) {
            echo "<script>
                alert('Email đã tồn tại, vui lòng update lại!');
                window.location.href='read';
                </script>";
        } else {
            header("location:read");
        }
        return $this->loadView('Crud.php');
    }

    public function updateUser($id)
    {
        $name = $_POST['name'] ?? null;
        $user = new UserModel();
        $user = $user->updateDetailUser($id, $name);
        header("location:http://myframework.local/users/read");
    }

    public function deleteUser($id)
    {
        $delete = new UserModel();
        $delete->modelDelete($id);
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode(["status" => "Success"]);
    }

}


?>