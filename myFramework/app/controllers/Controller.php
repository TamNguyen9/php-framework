<?php
namespace app\controllers;

use app\models\Model;

class Controller
{
    public $fileName = NULL;
    public $fileLayout = NULL;
    public $view = NULL;

    //tao ham load view 
    public function loadView($fileName, $data = [])
    {
        if (!empty($data))
            extract($data);
        //kiem tra xem duong dan file ton tai khong
        if (file_exists("../views/$fileName")) {
            $this->fileName = $fileName;
            //doc doi noi cua $fileName gan vao mot bien
            ob_start();
            include "../views/$fileName";
            $this->view = ob_get_contents();
            ob_get_clean();
            //kiem tra neu bien $this->fileLayout khong NULL thi include no
            if ($this->fileLayout != NULL)
                include "../views/$this->fileLayout";
            else
                echo $this->view;
        }
    }

//    public function checkLogin(){
//        $nodel = new Model();
//        $nodel->login();
//    }
}

?>

