<?php

namespace app\controllers;
use app\models\LoginModel;

class LoginController extends Controller{
    public function index(){
        return $this->loadView('LoginView.php');
    }

    // đăng nhập
    public function login(){
        $model = new LoginModel();
        $model->login();
//        header("location:http://myframework.local/users/login");
//        return $this->loadView("LoginView.php");
    }

    // đăng xuất
    public function logout(){
        // hủy biến session
        unset($_SESSION['email']);
        //redirect
        header("location:http://myframework.local/users/login");
    }
}
