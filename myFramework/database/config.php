<?php
namespace database;
class Config {
    protected $hostName;
    protected $userName;
    protected $dbPassWord;
    protected $dbName;

    public function __construct() {
        $this->hostName = 'localhost';
        $this->userName = 'root';
        $this->dbPassWord = '';
        $this->dbName = 'tam_project';
    }
}
?>