<?php

namespace database;

use database\Config;
use PDO;

class DB extends Config
{

    public $conn;  // biến kết nối CSDL

    // ham kết nối CSDL
    public function dbConnect()
    {
        $this->conn = new PDO("mysql:host=$this->hostName;dbname=$this->dbName;charset=utf8", $this->userName, $this->dbPassWord);
        //lay ket qua tra ve theo object
        $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        return $this->conn;
    }
}
?>