<?php

namespace router;

$router = new \router\Router();
$router->register('get', '/user/detail/1', ['UserController', 'detail'], [\app\middlewares\LogMiddleware::class]);
$router->register('get', '/user/{name}/{id}', ['UserController', 'detail']);
$router->register('get', '/users/login', ['LoginController', 'index']);
$router->register('post', '/users/login', ['LoginController', 'login']);
$router->register('get', '/users/logout', ['LoginController', 'logout']);
$router->register('get', '/users/read', ['UserController', 'read']);
$router->register('post', '/users/update/{id}', ['UserController', 'updateUser']);
$router->register('post', '/users/add', ['UserController', 'addUser']);
$router->register('post', '/users/delete/{id}', ['UserController', 'deleteUser']);

//route API
$router->register('get', '/api/users/read', ['api\UserController', 'read']);
$router->register('get', '/api/users/delete/{id}', ['api\UserController', 'deleteUser']);

try {
    $router->matchController();
} catch (\Exception $exception) {
    echo $exception->getMessage();
    exit();
}


?>